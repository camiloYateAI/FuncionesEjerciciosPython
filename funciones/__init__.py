'''
@note: Creado  el 15 de Febrero de 2018

@author: Camilo Yate

@organization: cyateya35439@universidadean.edu.co
@version: 1.0.0
@note: clase funciones con diferentes metodos
'''
import math

class funciones():
    
    
    '''
    (num,num) -> float
    Calcula la propina de una cuenta
    >>> Propina( 5000, 10)
    100.0
    >>> Propina( 10000, 10)
    1000.0
    @param :  cuenta: el valor total >> propina: porcentaje a cancelar de propina con la cuenta
    @return: el valor de la propina
    '''
   
   
    '''
    @var total_valor: inicializamos la variale a cero 
    '''
    total_valor = 0
    
    '''
    @postcondition: el metodo porpina recibe dos parametros 
    '''
    def Propina(self, cuenta, propina):
        
        '''
        @precondition: con los dos parametros recibidos se genera la
        multiplicacion y se divide en 100, para ese valor asignarselo
        a la variable
        '''
        total_valor = (cuenta*propina)/100
        
        '''
        @return: cuando se genera la operacion guarda ese valor y al momento de instanciar
        la funcion, nos retorna el valor de la propina
        '''
        return total_valor
       
       
    """ _________________________________________________________________________ """
   
    """
    (num,num) -> float
    Calcula la tasa de cambio de pesos a dolares
    >>> Tasa_cambio( 2000, 5000):
    10000000
    >>> Tasa_cambio( 2000, 300):
    600000
    @param :  pesos: la cantidad de pesos colombianos  >> dolares: tasa de cambio de una moneda a otra
    @return: valor de la tasa de cambios 
    """
      
    def Tasa_cambio(self, pesos, dolares):
        
        """
        @precondition: la funcion recibe dos parametro la cantidad de pesos colombianos
        que quiere cambiar y la tasa de camibio actual para hacer el calculo
        """
      
        cambio = (pesos*dolares)
        return cambio
    
    """ _________________________________________________________________________ """
    
    """
    (num) -> float
    Hacer la conversion de kelvin a centirados
    >>> Kelvin_centirados(123):
    -150  Grados celsius
    >>> Kelvin_centirados(13):
    -260  Grados celsius
    @param :  t_kelvin: como parametro necesitamos hacer los grados kelvin para la conversion
    @return: Grados celsius
    """
    
    def Kelvin_centirados(self,t_kelvin):
        
        """
        @precondition: la funcion recibe los grados kelvin y la operacion genera una resta
        con el valor de grados celsius y como puede en algunos calculos dar numeros
        muy grandes entonces se redondea el numero, se asigna a la variable conversion
        y se retorna a la funcions
        
        """
        
        conversion = (t_kelvin - 273.15.__round__())
        return conversion
        
        
        """ _________________________________________________________________________ """
        
        
    """
    (num) -> float
    calcular recorrido de kilometros de acuerdo a los galones
    >>> eficiencia_combustible( 19):
    743.375 Galones
    >>> eficiencia_combustible( 19):
    1173.75 Kilometros
    @param :  galones: con los galones vamos a generar el calculo 
    @return: kilometros recorridos
    """
          
    def eficiencia_combustible(self, galones):
        calculo = ((313 * galones)/8)
        return calculo
    
    
    
    
        """ _________________________________________________________________________ """
        
    
    """
    (num,num,num) -> float
    algoritmo para calcular regla de tres
    >>> regla_tres(2,4,7):
    14.0
    >>> regla_tres(5,6,8):
    9.6
    @param :  a,b,c: valores para la operacion de el 4 elemento
    @return: resultado el cuarto valor 
    """
        
    def regla_tres(self,a,b,c):
        resultado = ((b*c)/a)
        return resultado
    
    
    
    """ _________________________________________________________________________ """
    
    """
    (num,num) -> integer
    hacer el calculo de una venta neta de sillas y mesas 
    >>> mesas_sillas(2 sillas,3 mesas):
    211
    >>> mesas_sillas(4 sillas,3 mesas):
    311
    @param :cantidad_sillas,cantidad_mesas : el cliente asigna cuantas cantidas compro de cada producto 
    @return: venta neta 
    """
    def mesas_sillas(self,cantidad_sillas,cantidad_mesas):
        mesa = 50
        silla = 37
        venta_mesas = (mesa * cantidad_sillas)
        venta_sillas = (silla * cantidad_mesas)
        venta_neta = (venta_mesas + venta_sillas)
        return venta_neta
   

       
        
    """ _________________________________________________________________________ """
    
    
    """
    (num) -> integer
    algoritmo que de aceurdo a la hora del dia, saludar
    >>> saludo(10):
    "Buenos dias"
    >>> saludo(18):
    buenas tardes
    @param :hora, el programa necesita saber que hora es para indicar el mensaje
    @return: mensaje
    """
    
    
    
    """
        @precondition: en la funcion tenemos una condicionale anidada donde de acuerdo a la hora 
        indicada por el usuario en valida si la condicion es verdadera. y retorna un mensaje
        con el rando de horas 
        
         
    """
    #hora = int(input("ingresa tu hora "))
    def saludo(self,hora):
        if hora < 12:
            resultado =  print("Buenos dias")
            return resultado
            
        elif hora >= 12 and hora <= 19:
            resultado1 =  print("Buenas tardes")
            return resultado1
        elif hora >= 19 and hora <= 12:
            resultado2 =  print("Buenas noches")
            return resultado2
            
        else:
            print("Esa hora no existe en colombia")
                
         
         
         
        """ _________________________________________________________________________ """
     
     
    """
    (num) -> float
    algoritmo para calcular el radio de un circulo
    >>> area_circulo(1):
    El area del circulo es : 3.141592653589793
    >>> area_circulo():
    buenas tardes
    @param :radio : radio del circulo
    @return:area del circulo
    """ 
        
    def area_circulo(self,radio):
        return (math.pi * radio * radio)
         
         
        """ LLAMADO DE FUNCIONES CREANDO UNA INSTANCIA """

llamandofuncion = funciones()
print("La propina de esta cuenta es:", llamandofuncion.Propina(10000, 10))
print("Tu cambio de dolares a pesos fue", llamandofuncion.Tasa_cambio(2000, 300), " Pesos")
print("Conversion de kelvin a centigrados", llamandofuncion.Kelvin_centirados(13), " Grados celsius")
print("Recorriste", llamandofuncion.eficiencia_combustible(30), "Kilometros")
print("El resultado de la regla de tres es " , llamandofuncion.regla_tres(5, 6, 8))
print("la venta total de mesas y sillas fue ", llamandofuncion.mesas_sillas(4, 3))
print( llamandofuncion.saludo(hora = int(input("ingresa tu hora"))))
print("El area del circulo es :", llamandofuncion.area_circulo(10))

#JOSE CORDOBA ____________________________________________________________________________________


def es_par(numero):
    '''
    (int) -> boolean
    Valida si un numero es par
    >>> es_par(2)
    true
    >>> es_par(9)
    false
    :param numero: el numero a verificar
    :return: true si el numero es par false de lo contrario
    '''
    return numero%2==0

numero = int(input("Digite su numero:\n"))

#if nos permite tomar una decision
if(es_par(numero)):
    print("El número "+ str(numero) + " es par")
#else nos permite evaluar el caso contrario
else:
    print("El número "+ str(numero) + " no es par")

def saludar_hora(hora):
    '''
    (num) -> str
    Saluda de acuerdo a la hora dada
    Precondicion: hora 0 - 23
    >>> saludar_hora(10)
    "Buenos días"
    >>> saludar_hora(16)
    "Buenas tardes"
    >>> saludar_hora(21)
    "Buenas noches"
    :param hora: la hora actual
    :return: un saludo
    '''
    if hora<13:
        return "Buenos Días"
    elif hora < 17:
        return "Buenas tardes"
    else:
        return "Buenas Noches"

print(saludar_hora(int(input("Que hora es?\n"))))
